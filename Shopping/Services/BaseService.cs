using Shopping.Models;

namespace Shopping.Services
{
    public class BaseService
    {
        protected ShoppingContext _dbContext;

        public BaseService(ShoppingContext dbContext)
        {
            _dbContext = dbContext;
        }

    }
}
