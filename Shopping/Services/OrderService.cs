using Microsoft.EntityFrameworkCore;
using Shopping.Models;

namespace Shopping.Services
{
    public class OrderService : BaseService
    {
        public OrderService(ShoppingContext dbContext) : base(dbContext) { }

        public Order AddOrder(Customer customer, Supplier supplier, Payment payment, OrderDesciption orderDesciption)
        {
            Order order = new Order();
            order.CustomerOwner = customer;
            order.SupplierOwner = supplier;
            order.PaymentMethod = payment;
            order.Desciption = orderDesciption;
            
            _dbContext.Orders.Add(order);
            if (_dbContext.SaveChanges() == 0)
                throw new Exception("create order fail");

            return order;
        }

        public List<Order> GetOrdersByCustomerID(string customerID)
        {
            var orders = _dbContext.Orders.Include(o => o.CustomerOwner).ThenInclude(c => c.Description).Include(o => o.SupplierOwner).ThenInclude(s => s.Description).Include(o => o.Desciption).Include(o => o.PaymentMethod).Where(o => o.CustomerOwner.CustomerID.Equals(customerID)).ToList();
            if (orders == null)
                throw new Exception("orders not find");

            return orders;
        }

        public List<Order> GetOrdersBySupplierID(string supplierID)
        {
            var orders = _dbContext.Orders.Include(o => o.SupplierOwner).ThenInclude(s => s.Description).Include(o => o.CustomerOwner).ThenInclude(c => c.Description).Include(o => o.Desciption).Include(o => o.PaymentMethod).Where(o => o.SupplierOwner.SupplierID.Equals(supplierID)).ToList();
            if (orders == null)
                throw new Exception("orders not find");

            return orders;
        }

        public void FinishOrderByOrderID(int orderID)
        {
            var order = _dbContext.Orders.Find(orderID);
            if (order == null)
                throw new Exception("order not find");

            order.OrderStatus = 1;

            if (_dbContext.SaveChanges() == 0)
                throw new Exception("finish order fail");
                        
        }

        public void CancelOrderByOrderID(int orderID)
        {
            var order = _dbContext.Orders.Find(orderID);
            if (order == null)
                throw new Exception("order not find");

            order.OrderStatus = 2;

            if (_dbContext.SaveChanges() == 0)
                throw new Exception("finish order fail");
        }

    }
}