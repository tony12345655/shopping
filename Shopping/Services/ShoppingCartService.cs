using Microsoft.EntityFrameworkCore;
using Shopping.Models;

namespace Shopping.Services
{
    public class ShoppingCartService : BaseService
    {
        public ShoppingCartService(ShoppingContext dbContext) : base(dbContext) { }

        public ICollection<Item> getItemsByCustomerID(string CustomerID)
        {
            var customer = _dbContext.Customers.Include(c => c.ShoppingCart).ThenInclude(s => s.Items).ThenInclude(i => i.Product).FirstOrDefault(c => c.CustomerID.Equals(CustomerID));
            if (customer == null)
                throw new Exception("custoemr not find");
            
            return customer.ShoppingCart.Items;
        }


        public void addItemsToShoppingCartByCustomerID(string CustomerID, ICollection<Item> items)
        {
            var customer = _dbContext.Customers.Include(c => c.ShoppingCart).ThenInclude(s => s.Items).FirstOrDefault(c => c.CustomerID.Equals(CustomerID));
            if (customer == null)
                throw new Exception("custoemr not find");

            foreach (var item in items)
                customer.ShoppingCart.Items.Add(item);

            if (_dbContext.SaveChanges() == 0)
                throw new Exception("add items fail");
        }

        public Customer clearShoppingCartItemsByCustomerID(string customerID)
        {
            var customer = _dbContext.Customers.Include(c => c.ShoppingCart).ThenInclude(s => s.Items).FirstOrDefault(c => c.CustomerID.Equals(customerID));
            if (customer == null)
                throw new Exception("custoemr not find");

            customer.ShoppingCart.Items.Clear();
            if (_dbContext.SaveChanges() == 0)
                throw new Exception("clear items fail");

            return customer;
        }

        public void removeShoppingCartItemsByItemsID(int itemID)
        {
            var item = _dbContext.Items.Find(itemID);
            if (item == null)
                throw new Exception("item not find");
            _dbContext.Remove(item);

        }
    }
}