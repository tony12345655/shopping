using Microsoft.EntityFrameworkCore;
using Shopping.Models;
using Shopping.Dtos;

namespace Shopping.Services
{
    public class ProductService : BaseService
    {
        public ProductService(ShoppingContext dbContext) : base(dbContext) { }

        public List<Product> GetAllProducts()
        {
            List<Product> products = _dbContext.Products.Select(p => p).ToList();

            return products;
        }

        public List<Product> GetProductsByProductIDs(List<ItemDto> itemDtos)
        {
            List<Product> products = new List<Product>();
            foreach(var itemDto in itemDtos)
            {
                var product = _dbContext.Products.Find(itemDto.ProductID);
                if (product == null)
                    throw new Exception("product not find");
                products.Add(product);
            }
            return products;
        }

        public Supplier AddProduct(Supplier supplier, ProductDto productDto)
        {   
            Product product = new Product {SupplierID = supplier.SupplierID, ProductName = productDto.ProductName, Price = productDto.Price, RemainingAmount = productDto.RemainingAmount};
            supplier.Products.Add(product);
            if (_dbContext.SaveChanges() == 0)
                throw new Exception("add product fail");

            return supplier;
        }
        
        public ICollection<Product> GetSupplierProductsBySupplierID(string SupplierID)
        {
            var supplier = _dbContext.Suppliers.Include(s => s.Products).FirstOrDefault(s => s.SupplierID.Equals(SupplierID));

            if (supplier == null)
                throw new Exception("supplier not find");

            return supplier.Products;
        }

        public void ChangeProductQuantity(int productID, int num)
        {
            var product = _dbContext.Products.Find(productID);

            if (product == null)
                throw new Exception("product not find");
            
            product.RemainingAmount = num;

            if (_dbContext.SaveChanges() == 0)
                throw new Exception("change quantity fail");

        }
    }
}