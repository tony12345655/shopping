using Shopping.Models;
using Shopping.Dtos;

namespace Shopping.Services
{
    public class ItemService : BaseService
    {
        public ItemService(ShoppingContext dbContext) : base(dbContext) { }

        public ICollection<Item> CreateItems(List<Product> products, List<ItemDto> itemDtos)
        {
            ICollection<Item> items = new List<Item>();
            for (int index = 0; index < products.Count; index++)
            {
                Item item  = new Item{Product = products[index], Quantity = itemDtos[index].Quantity};
                items.Add(item);
            }

            return items;
                
        } 

    }
}