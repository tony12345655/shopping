using Shopping.Models;

namespace Shopping.Services
{
    public class PaymentService : BaseService
    {
        public PaymentService(ShoppingContext dbContext) : base(dbContext) { }

        public Payment GetPaymentByPaymentID(int PaymentID)
        {
            var payment = _dbContext.Payments.Find(PaymentID);
            if (payment == null)
                throw new Exception("payment not find");

            return payment;
        }
    }
}