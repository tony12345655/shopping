using Microsoft.EntityFrameworkCore;
using Shopping.Models;

namespace Shopping.Services
{
    public class SupplierService : BaseService
    {
        public SupplierService(ShoppingContext dbContext) : base(dbContext) { }

        public Supplier AddSupplier(Supplier supplier)
        {
            _dbContext.Suppliers.Add(supplier);
            if (_dbContext.SaveChanges() == 0)
                throw new Exception("create supplier fail");

            return supplier;
        }

        public Supplier GetSupplierBySupplierID(string supplierID)
        {
            var supplier = _dbContext.Suppliers.Include(s => s.Description).FirstOrDefault(s => s.SupplierID.Equals(supplierID));
            if (supplier == null)
                throw new Exception("supplier not found!");

            return supplier;
        }

        public Supplier UpdateSupplier(Supplier supplier)
        {
            _dbContext.Suppliers.Update(supplier);
            if (_dbContext.SaveChanges() == 0)
                throw new Exception("update supplier fail");

            return supplier;
        }

        public bool CheckSupplier(string supplierID, string password)
        {
            bool status = false;
            var supplier = _dbContext.Suppliers.Find(supplierID);
            if (supplier != null && supplier.Password == password)
                status = true;

            return status;
        }

    }
}
