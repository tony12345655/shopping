using Microsoft.EntityFrameworkCore;
using Shopping.Models;

namespace Shopping.Services
{
    public class CustomerService : BaseService
    {
        public CustomerService(ShoppingContext dbContext) : base(dbContext) { }

        
        public Customer GetCustomerByCustomerID(string customerID)
        {
            var customer = _dbContext.Customers.Include(c => c.Description).FirstOrDefault(c => c.CustomerID.Equals(customerID));
            if (customer == null)
                throw new Exception("customer not found!");

            return customer;
        }
        
        public Customer AddCustomer(Customer customer)
        {
            _dbContext.Customers.Add(customer);
            if (_dbContext.SaveChanges() == 0)
                throw new Exception("create customer fail");

            return customer;
        }

        public Customer UpdateCustomer(Customer customer)
        {
            _dbContext.Customers.Update(customer);
            if (_dbContext.SaveChanges() == 0)
                throw new Exception("update customer fail");

            return customer;
        }

        public bool CheckCustomer(string customerID, string password)
        {
            bool status = false;
            var customer = _dbContext.Customers.Find(customerID);
            if (customer != null && customer.Password == password)
                status = true;

            return status;
        }

        public void AddItemToShoppingCart(Customer customer, Item item)
        {   
            customer.ShoppingCart.Items.Add(item);
            if (_dbContext.SaveChanges() == 0)
                throw new Exception("add item fail");
        }

    }
}
