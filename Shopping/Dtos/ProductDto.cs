using Shopping.Models;

namespace Shopping.Dtos
{
    public class ProductDto
    {
        public string SupplierID {get; set;} = null!;

        public string ProductName {get; set;} = null!;

        public int Price {get; set;}

        public int RemainingAmount  {get; set;}
    }
}