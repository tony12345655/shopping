using Shopping.Models;

namespace Shopping.Dtos
{
    public class ProductQuantityDto
    {
        public int ProductID {get; set;}

        public int Quantity {get; set;}
    }
}