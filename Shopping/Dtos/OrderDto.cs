using Shopping.Models;

namespace Shopping.Dtos
{
    public class OrderDto
    {
        public string CustomerID {get; set;} = null!;

        public string SupplierID {get; set;} = null!;

        public int PaymentID {get; set;}

        public OrderDesciption Desciption {get; set;} = null!;
    }
}