namespace Shopping.Dtos
{
    public class ItemDto
    {
        public int ProductID {get; set;}

        public int Quantity {get; set;}
    }

}