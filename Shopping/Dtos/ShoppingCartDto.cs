using Shopping.Models;

namespace Shopping.Dtos
{
    public class ShoppingCartDto
    {
        public string CustomerID {get; set;} = null!;

        public List<ItemDto> ItemDtos {get; set;} = null!;
    }
}