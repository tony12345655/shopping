namespace Shopping.Dtos
{
    public class LoginDto
    {
        public string Account {get; set; } = null!;

        public string Password {get; set; } = null!;
    }
}