using Microsoft.AspNetCore.Mvc;
using Shopping.Models;
using Shopping.Services;
using Shopping.Dtos;

namespace shopping.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SupplierController : ControllerBase
    {

        private readonly SupplierService _supplierService;

        public SupplierController(ShoppingContext dbContext)
        {
            _supplierService = new SupplierService(dbContext);   
        }

        [HttpGet("{supplierID}")]
        public IActionResult GetSupplier(string supplierID)
        {
            Supplier supplier = _supplierService.GetSupplierBySupplierID(supplierID);
            return Ok(supplier);
        }

        [HttpPost]
        public IActionResult AddSupplier(Supplier supplier)
        {
            Supplier new_supplier = _supplierService.AddSupplier(supplier);
           
            return Ok(new_supplier);

        }

        [HttpPost("login")]
        public IActionResult CustomerLogin(LoginDto loginDto)
        {   
            bool loginStatus = _supplierService.CheckSupplier(loginDto.Account, loginDto.Password);
            ResponseDto responseDto = new ResponseDto();
            if (loginStatus) 
            {
                responseDto.Success = true;
                responseDto.Message = "登陸成功";
            }
            else
            {
                responseDto.Success = false;
                responseDto.Message = "帳號或密碼錯誤";
            }
                

            return Ok(responseDto);
        }

        [HttpPut]
        public IActionResult UpdateSupplier(Supplier supplier)
        {
            Supplier newSupplier = _supplierService.UpdateSupplier(supplier);
            return Ok(newSupplier);
        }
    }

}

