using Microsoft.AspNetCore.Mvc;
using Shopping.Models;
using Shopping.Dtos;
using Shopping.Services;

namespace shopping.Controllers
{
    [ApiController]
    [Route("[controller]")]

     public class OrderController : ControllerBase
     {
        private readonly CustomerService _customerService;

        private readonly SupplierService _supplierService;

        private readonly PaymentService _paymentService;

        private readonly OrderService _orderService;

        public OrderController(ShoppingContext dbContext)
        {
            _customerService = new CustomerService(dbContext);
            _supplierService = new SupplierService(dbContext);
            _paymentService = new PaymentService(dbContext);
            _orderService  = new OrderService(dbContext);
        }

        [HttpGet("customer/{customerID}")]
        public IActionResult GetCustomerOrders(string customerID)
        {
            List<Order> orders = _orderService.GetOrdersByCustomerID(customerID);

            return Ok(orders);
        }

        [HttpGet("supplier/{supplierID}")]
        public IActionResult GetSupplierOrders(string supplierID)
        {
            List<Order> orders = _orderService.GetOrdersBySupplierID(supplierID);

            return Ok(orders);
        }

        [HttpGet("finish/{orderID}")]
        public IActionResult FinishOrder(int orderID)
        {
            _orderService.FinishOrderByOrderID(orderID);
        
            return Ok(new ResponseDto{Success = true, Message = "訂單完成"});
        }

        [HttpGet("cancel/{orderID}")]
        public IActionResult CancelOrder(int orderID)
        {
            _orderService.CancelOrderByOrderID(orderID);

            return Ok(new ResponseDto{Success = true, Message = "訂單取消"});
        }

        [HttpPost]
        public IActionResult AddOrder(OrderDto orderDto)
        {
            Customer customer = _customerService.GetCustomerByCustomerID(orderDto.CustomerID);
            Supplier supplier = _supplierService.GetSupplierBySupplierID(orderDto.SupplierID);
            Payment payment = _paymentService.GetPaymentByPaymentID(orderDto.PaymentID);
            _orderService.AddOrder(customer, supplier, payment, orderDto.Desciption);

            return Ok(new ResponseDto{Success = true, Message = "訂單新增成功"});
        }

     }
}