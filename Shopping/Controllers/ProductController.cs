using Microsoft.AspNetCore.Mvc;
using Shopping.Models;
using Shopping.Dtos;
using Shopping.Services;

namespace shopping.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {


        private readonly SupplierService _supplierService;
        private readonly ProductService _productService;

        public ProductController(ShoppingContext dbContext)
        {
            _supplierService = new SupplierService(dbContext);
            _productService = new ProductService(dbContext);
        }

        [HttpGet]
        public IActionResult GetProducts ()
        {
            List<Product> products = _productService.GetAllProducts();
            return Ok(products);
        }

        [HttpGet("{supplierID}")]
        public IActionResult GetSupplierProducts (string supplierID)
        {
            ICollection<Product> products = _productService.GetSupplierProductsBySupplierID(supplierID);
            return Ok(products);
        }

        [HttpPost]
        public IActionResult AddProduct (ProductDto productDto)
        {
            Supplier supplier = _supplierService.GetSupplierBySupplierID(productDto.SupplierID);  
            _productService.AddProduct(supplier, productDto);
            return Ok(new ResponseDto{Success = true, Message = "商品新增成功"});
        }

        [HttpPost("change")]
        public IActionResult ChangeProductQuantity (ProductQuantityDto productQuantityDto)
        {
            _productService.ChangeProductQuantity(productQuantityDto.ProductID, productQuantityDto.Quantity);

            return Ok(new ResponseDto{Success = true, Message = "商品數量更改成功"});
        }

    }

}

