using Microsoft.AspNetCore.Mvc;
using Shopping.Models;
using Shopping.Services;
using Shopping.Dtos;

namespace shopping.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomerController : ControllerBase
    {

        private readonly CustomerService _customerService;

        public CustomerController(ShoppingContext dbContext)
        {
            _customerService = new CustomerService(dbContext);
        }

        [HttpGet("{customerID}")]
        public IActionResult GetCustomer(string customerID)
        {
            Customer customer = _customerService.GetCustomerByCustomerID(customerID);
            return Ok(customer);
        }

        [HttpPost]
        public IActionResult AddCustomer(Customer customer)
        {

            Customer newCustomer = _customerService.AddCustomer(customer);
          
            return Ok(newCustomer);

        }

        [HttpPost("login")]
        public IActionResult CustomerLogin(LoginDto loginDto)
        {   
            bool loginStatus = _customerService.CheckCustomer(loginDto.Account, loginDto.Password);
            ResponseDto responseDto = new ResponseDto();
            if (loginStatus) 
            {
                responseDto.Success = true;
                responseDto.Message = "登陸成功";
            }
            else
            {
                responseDto.Success = false;
                responseDto.Message = "帳號或密碼錯誤";
            }
                

            return Ok(responseDto);
        }

        [HttpPut]
        public IActionResult UpdateCustomer(Customer customer)
        {
            Customer newCustomer = _customerService.UpdateCustomer(customer);
            return Ok(newCustomer);
        }
    }

}

