using Microsoft.AspNetCore.Mvc;
using Shopping.Models;
using Shopping.Services;
using Shopping.Dtos;

namespace shopping.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class ShoppingCartController : ControllerBase
    {
        private readonly CustomerService _customerService;
        private readonly ProductService _productService;
        private readonly ItemService _itemService;
        private readonly ShoppingCartService _shoppingCartService;

        public ShoppingCartController(ShoppingContext dbContext)
        {
            _customerService = new CustomerService(dbContext);
            _productService = new ProductService(dbContext);
            _itemService = new ItemService(dbContext);
            _shoppingCartService = new ShoppingCartService(dbContext);
        }

        [HttpGet("{customerID}")]
        public IActionResult getShoppingCartItemsBuCusomerID(string customerID)
        {
            ICollection<Item> items = _shoppingCartService.getItemsByCustomerID(customerID);

            return Ok(items);
        }

        [HttpPost]
        public IActionResult addItemsToShoppingCart(ShoppingCartDto shoppingCartDto)
        {   
            List<Product> products = _productService.GetProductsByProductIDs(shoppingCartDto.ItemDtos);
            ICollection<Item> items = _itemService.CreateItems(products, shoppingCartDto.ItemDtos);
            _shoppingCartService.addItemsToShoppingCartByCustomerID(shoppingCartDto.CustomerID, items);

            return Ok(new ResponseDto{Success = true, Message = "商品成功進入購物車成功"});
        }

        [HttpPut("clear/{customerID}")]
        public IActionResult clearShoppingCartItems(string customerID)
        {
            _shoppingCartService.clearShoppingCartItemsByCustomerID(customerID);

            return Ok(new ResponseDto{Success = true, Message = "購物車清除成功"});
        }

        [HttpDelete("{itemID}")]
        public IActionResult deleteShoppingCartItem(int itemID)
        {
            _shoppingCartService.removeShoppingCartItemsByItemsID(itemID);
            return Ok(new ResponseDto{Success = true, Message = "購物車商品刪除成功"});
        }
    }
}