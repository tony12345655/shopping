namespace Shopping.Models
{

    public class Product
    {
        public int ProductID {get; set;}

        public string SupplierID {get; set;} = null!;

        public string ProductName {get; set;} = null!;
        
        public int Price {get; set;}

        public int RemainingAmount {get; set;}
        
    }
}