namespace Shopping.Models
{
    public class Customer
    {
        public string CustomerID {get; set;} = null!;

        public string Password {get; set;} = null!;

        public CustomerDescription Description {get; set;} = null!;

        public ShoppingCart ShoppingCart {get;set;} = new ShoppingCart();
    }
}