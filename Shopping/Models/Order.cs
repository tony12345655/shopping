namespace Shopping.Models
{
    public class Order
    {

        public int OrderID {get; set;}

        public OrderDesciption Desciption {get; set;} = null!;

        public Customer CustomerOwner {get; set;} = null!;

        public Supplier SupplierOwner {get; set;} = null!;

        public Payment PaymentMethod {get; set;} = null!;

        public int OrderStatus {get; set; }



    }
}
