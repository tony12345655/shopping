namespace Shopping.Models
{

    public class CustomerDescription
    {
        public int DescriptionID {get; set;}

        public string Name {get; set;} = null!;

        public string Gender {get; set;} = null!;
        
        public string PhoneNumber{get; set;}  = null!;

        public string Address{get; set;}  = null!;
        
    }
}