namespace Shopping.Models
{

    public class SupplierDescription
    {
        public int DescriptionID {get; set;}
        
        public string Name {get;set;}  = null!;

        public string PhoneNumber {get; set;}  = null!;

        public string Address {get; set;}  = null!;

        public string UniformNumber {get; set;} = null!; 
    }
}