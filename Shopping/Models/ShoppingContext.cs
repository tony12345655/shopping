using Microsoft.EntityFrameworkCore;

namespace Shopping.Models;

public partial class ShoppingContext : DbContext
{
    public ShoppingContext() {}

    public ShoppingContext(DbContextOptions<ShoppingContext> options) : base(options) {}

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-2T2VH19\\SQLEXPRESS;Database=Shopping;Trusted_Connection=True;TrustServerCertificate=True;User ID=sa;Password=1234");
        }

    }

     protected override void OnModelCreating(ModelBuilder modelBuilder)
        {


            modelBuilder.Entity<Customer>()
                .HasKey(c => c.CustomerID);

            modelBuilder.Entity<CustomerDescription>()
                .HasKey(c => c.DescriptionID);

            modelBuilder.Entity<CustomerDescription>()
                .Property(c => c.DescriptionID)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Supplier>()
                .HasKey(s => s.SupplierID);

            modelBuilder.Entity<SupplierDescription>()
                .HasKey(s => s.DescriptionID);

            modelBuilder.Entity<SupplierDescription>()
                .Property(s => s.DescriptionID)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Product>()
                .HasKey(p => p.ProductID);

            modelBuilder.Entity<Product>()
                .Property(p => p.ProductID)
                .ValueGeneratedOnAdd();
            
            modelBuilder.Entity<Product>()
                .Property(p => p.RemainingAmount)
                .HasDefaultValue(0);

            modelBuilder.Entity<Item>()
                .HasKey(i => i.ItemID);

            modelBuilder.Entity<Item>()
                .Property(i => i.ItemID)
                .ValueGeneratedOnAdd();
            
            modelBuilder.Entity<ShoppingCart>()
                .HasKey(s => s.ShoppingCartID);
            
            modelBuilder.Entity<ShoppingCart>()
                .Property(s => s.ShoppingCartID)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Order>()
                .HasKey(o => o.OrderID);

            modelBuilder.Entity<Order>()
                .Property(o => o.OrderID)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<OrderDesciption>()
                .HasKey(o => o.DescriptionID);

            modelBuilder.Entity<OrderDesciption>()
                .Property(o => o.DescriptionID)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Payment>()
                .HasKey(p => p.PaymentID);

            modelBuilder.Entity<Payment>()
                .Property(p => p.PaymentID)
                .ValueGeneratedOnAdd();

        }

    public virtual DbSet<Customer> Customers {get; set;} = null!;
    public virtual DbSet<CustomerDescription> CustomerDescriptions {get; set;} = null!;
    public virtual DbSet<Supplier> Suppliers {get; set;} = null!;
    public virtual DbSet<SupplierDescription> SupplierDescriptions {get; set;} = null!;
    public virtual DbSet<Product> Products {get; set;} = null!;
    public virtual DbSet<Item> Items {get; set;} = null!;
    public virtual DbSet<ShoppingCart> ShoppingCarts {get; set;} = null!;
    public virtual DbSet<Order> Orders {get; set;} = null!;
    public virtual DbSet<OrderDesciption> OrderDesciptions {get; set;} = null!;
    public virtual DbSet<Payment> Payments {get; set;} = null!;

}