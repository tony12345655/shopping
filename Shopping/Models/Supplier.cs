namespace Shopping.Models
{

    public class Supplier
    {
        public string SupplierID {get; set;} = null!;

        public string Password {get; set;} = null!;

        public SupplierDescription Description {get; set;} = null!;

        public ICollection<Product> Products {get; } = new List<Product>();
        
    }
}