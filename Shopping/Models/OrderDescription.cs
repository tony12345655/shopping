namespace Shopping.Models
{

    public class OrderDesciption
    {

        public int DescriptionID {get; set;}
        
        public string ItemsName {get; set;} = null!;

        public int TotalPrice {get; set;}

        public DateTime CreatTime {get; set;}

        public OrderDesciption()
        {
            this.CreatTime = DateTime.UtcNow.AddHours(8);
        }

    }
}