namespace Shopping.Models
{

    public class Item
    {
        public int ItemID {get; set;}

        public Product Product {get; set;} = null!;

        public int Quantity {get; set;}
    }
}