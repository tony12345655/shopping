namespace Shopping.Models
{

    public class ShoppingCart
    {
        public int ShoppingCartID {get; set;}

        public ICollection<Item> Items {get; set;} = new List<Item>();

    }
}