using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Shopping.Models;
using NSubstitute;
using NUnit.Framework;


namespace Shopping.Tests.Controllers 
{
    public class OrderTest
    {   

        private Order _order;

        [SetUp]
        public void SetUp(){
            _order = new Order();
        }


        [Test]
        public void SetOrderID(){
            int orderid = 1;
            _order.OrderID = orderid;

            int test_orderid = _order.OrderID;

            Assert.AreEqual(orderid,test_orderid);
            Assert.AreNotEqual(2, test_orderid);
        }
    }
}
