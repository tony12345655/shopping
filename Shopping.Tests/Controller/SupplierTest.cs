using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Shopping.Models;
using NSubstitute;
using NUnit.Framework;


namespace Shopping.Tests.Controllers 
{
    public class SupplierTest
    {   

        private Supplier _supplier;

        [SetUp]
        public void SetUp(){
            _supplier = new Supplier();
        }


        [Test]
        public void SetPassword(){
            string password = "Password123!";
            _supplier.Password = password;

            string test_password = _supplier.Password;

            Assert.AreEqual(password,test_password);
            Assert.AreNotEqual("00000000", test_password);
        }
    }
}
