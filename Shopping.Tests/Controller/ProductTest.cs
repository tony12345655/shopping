using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Shopping.Models;
using NSubstitute;
using NUnit.Framework;


namespace Shopping.Tests.Controllers 
{
    public class ProductTest
    {
        private  Product _Product;

        [SetUp]
        public void SetUp()
        {
            _Product = new Product();
        }


        [Test]
        public void SetProductID()
        {
            int Productid = 1;
            _Product.ProductID = Productid;

            int test_Productid = _Product.ProductID;

            Assert.AreEqual(Productid, test_Productid);
            Assert.AreNotEqual(2, test_Productid);
        }

        [Test]
        public void SetProductName()
        {
            string ProductName = "演唱會門票";
            _Product.ProductName = ProductName;

            string test_ProductName = _Product.ProductName;

            Assert.AreEqual(ProductName,test_ProductName);
            Assert.AreNotEqual("00000000",test_ProductName);
        }

        [Test]
        public void SetPrice()
        {
            int Price = 399;
            _Product.Price = Price;

            int test_Price = _Product.Price;

            Assert.AreEqual(Price,test_Price);
            Assert.AreNotEqual(1234,test_Price);
        }

        [Test]
        public void SetRemainingAmount()
        {
            int RemainingAmount = 99;
            _Product.RemainingAmount = RemainingAmount;

            int test_RemainingAmount = _Product.RemainingAmount;

            Assert.AreEqual(RemainingAmount,test_RemainingAmount);
            Assert.AreNotEqual(1234,test_RemainingAmount);
        }
    }
}