using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Shopping.Models;
using NSubstitute;
using NUnit.Framework;


namespace Shopping.Tests.Controllers 
{
    public class ItemTest
    {
        private  Item _Item;

        [SetUp]
        public void SetUp()
        {
            _Item = new Item();
        }


        [Test]
        public void SetItemID()
        {
            int Itemid = 1;
            _Item.ItemID = Itemid;

            int test_Itemid = _Item.ItemID;

            Assert.AreEqual(Itemid, test_Itemid);
            Assert.AreNotEqual(2, test_Itemid);
        }


        [Test]
        public void SetItemQuantity()
        {
            int totalQuantity = 10;
            _Item.Quantity = totalQuantity;


            Assert.AreEqual(totalQuantity,_Item.Quantity);
            Assert.AreNotEqual(1234,_Item.Quantity);

        }
    }
}