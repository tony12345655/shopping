using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Shopping.Models;
using NSubstitute;
using NUnit.Framework;


namespace Shopping.Tests.Controllers 
{
    public class CustomerDescriptionTest
    {   

        private CustomerDescription _CuDe;

        [SetUp]
        public void SetUp()
        {
            _CuDe = new CustomerDescription();
        }

        [Test]
        public void SetUserName()
        {
            string username = "apple";
            _CuDe.Name = username;

            string test_username = _CuDe.Name;

            Assert.AreEqual(username, test_username);
            Assert.AreNotEqual("banana", test_username);
        }

        [Test]
        public void SetUserAge()
        {
            int userage = 18;
            _CuDe.Age = userage;

            int test_userage = _CuDe.Age;

            Assert.AreEqual(userage, test_userage);
            Assert.AreNotEqual(20, test_userage);
        }

        [Test]
        public void SetUserPhoneNumber()
        {
            string userphonenumber = "0988888888";
            _CuDe.PhoneNumber = userphonenumber;

            string test_userphonenumber = _CuDe.PhoneNumber;

            Assert.AreEqual(userphonenumber, test_userphonenumber);
            Assert.AreNotEqual("0977777777", test_userphonenumber);
        }

        [Test]
        public void SetUserAddress()
        {
            string useraddress = "aaaaaaaaa";
            _CuDe.Address = useraddress;

            string test_useraddress = _CuDe.Address;

            Assert.AreEqual(useraddress, test_useraddress);
            Assert.AreNotEqual("bbbbbbbbb", test_useraddress);
        }


    }
}
