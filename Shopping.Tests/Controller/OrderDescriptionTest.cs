using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Shopping.Models;
using NSubstitute;
using NUnit.Framework;


namespace Shopping.Tests.Controllers 
{
    public class OrderDescriptionTest
    {   

        private OrderDesciption _OrDe;

        [SetUp]
        public void SetUp()
        {
            _OrDe = new OrderDesciption();
        }

        [Test]
        public void SetTotalPrice()
        {
            int totalprice = 1000;
            _OrDe.TotalPrice = totalprice;


            Assert.AreEqual(totalprice,_OrDe.TotalPrice);
            Assert.AreNotEqual(1234,_OrDe.TotalPrice);

        }

        [Test]
        public void SetCreateTime()
        {
            DateTime now = DateTime.Now;
            _OrDe.CreatTime = now;

            Assert.AreEqual(now,_OrDe.CreatTime);
            Assert.AreNotEqual(DateTime.Now,_OrDe.CreatTime);

        }
    }
}
