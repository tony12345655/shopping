using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Shopping.Models;
using NSubstitute;
using NUnit.Framework;


namespace Shopping.Tests.Controllers 
{
    public class PaymentTest
    {   

        private Payment _payment;

        [SetUp]
        public void SetUp(){
            _payment = new Payment();
        }


        [Test]
        public void SetID(){
            int id = 1;
            _payment.PaymentID = id;

            int test_id = _payment.PaymentID;

            Assert.AreEqual(_payment.PaymentID, test_id);
            Assert.AreNotEqual(2, test_id);
        }

        [Test]
        public void SetPaymentName()
        {
            string paymentname = "cash";
            _payment.Name = paymentname;

            Assert.AreEqual(paymentname, _payment.Name);
            Assert.AreNotEqual("credit car", _payment.Name);

        }

    }
}
