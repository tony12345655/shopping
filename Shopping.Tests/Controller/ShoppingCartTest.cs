using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Shopping.Models;
using NSubstitute;
using NUnit.Framework;


namespace Shopping.Tests.Controllers 
{
    public class ShoppingCartTest
    {
        private  ShoppingCart _ShoppingCart;

        [SetUp]
        public void SetUp()
        {
            _ShoppingCart = new ShoppingCart();
        }

        [Test]
        public void SetShoppingCartID()
        {
            int ShoppingCartID = 1;
            _ShoppingCart.ShoppingCartID = ShoppingCartID;

            int test_ShoppingCartID = _ShoppingCart.ShoppingCartID;

            Assert.AreEqual(ShoppingCartID, test_ShoppingCartID);
            Assert.AreNotEqual(2, test_ShoppingCartID);
        }
    }
}
