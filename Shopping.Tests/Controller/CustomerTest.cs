using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Shopping.Models;
using NSubstitute;
using NUnit.Framework;


namespace Shopping.Tests.Controllers 
{
    public class CustomerTest
    {   

        private Customer _Cu;

        [SetUp]
        public void SetUp()
        {
            _Cu = new Customer();
        }

        [Test]
        public void SetPassword()
        {
            string password = "abcdefg";
            _Cu.Password = password;

            string test_password = _Cu.Password;

            Assert.AreEqual(password,test_password);
            Assert.AreNotEqual("00000000",test_password);
        }


    }
}
